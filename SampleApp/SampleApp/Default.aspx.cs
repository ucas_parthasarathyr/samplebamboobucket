﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MyLibrary;

namespace SampleApp
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ResultContainer.Visible = false;
            container.Visible = true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            MyLibrary.Common commonObj=new Common();
            string fullName = commonObj.Concat(TxtFirstName.Text, TxtLastName.Text);
            lblName.Text="Welcome "+ fullName;
            container.Visible = false;
            ResultContainer.Visible = true;
        }
    }
}