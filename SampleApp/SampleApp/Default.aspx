﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SampleApp.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <style>
         #container {
    display: table;
    }

  #row  {
    display: table-row;
    }

  #left, #right, #middle {
    display: table-cell;
    }

    </style>
    <title>Welcome to Bamboo Build Plan</title>
    
</head>
<body>
    <form id="form1" runat="server">
    
   <div id="container" runat="server">
       <div>
    <h1>Welcome to Bamboo Build Plan</h1>
    </div>
  <div id="row">

  	<div id="left">
  		<h4>*First Name</h4>
  		<p><asp:TextBox ID="TxtFirstName" runat="server"></asp:TextBox></p>
  	</div>
  	<div id="middle">
    	<h4>*Last Name</h4>
    	<p><asp:TextBox ID="TxtLastName" runat="server"></asp:TextBox></p>
  	</div>
      <div id="right">
    	<h4>
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" /></h4>    	
  	</div>
	</div>

</div>
        <div id="ResultContainer" runat="server">

            <asp:Label ID="lblName" runat="server" Text="Label" Font-Bold="True" Font-Size="Large" ForeColor="Maroon"></asp:Label>

        </div>
    </form>
</body>
</html>
