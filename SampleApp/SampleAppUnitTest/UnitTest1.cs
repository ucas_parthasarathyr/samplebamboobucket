﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SampleAppUnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1_TestCase1()
        {
            MyLibrary.Common obj = new MyLibrary.Common();
            string result=obj.Concat("Hello", "World");
            Assert.AreEqual("HelloWorld", result);
        }

        [TestMethod]
        public void TestMethod1_TestCase2()
        {
            MyLibrary.Common obj = new MyLibrary.Common();
            string result = obj.Concat("Hello", "");
            Assert.AreEqual("Hello", result);
        }
    }
}
